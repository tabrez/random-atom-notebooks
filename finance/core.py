# %%
import pandas as pd
import datetime as dt
def nav_on(mf_code, date):
    """Return NAV of the mf_code as on start_date using quandl service.

    Use quandl's API to retrieve NAV of the fund specified by mf_code on
    start_date.

    Arguments:
    mf_code: code of the mutual fund whose NAV is retrieved
    start_date: NAV of this date is retrieved
    """
    url = ('https://www.quandl.com/api/v3/datasets/AMFI/' +
          str(mf_code) +
          '.csv?api_key=A-j1TXe6-_yv9sU7CRf2&limit=1&' +
          'start_date=' +
          str(date))
    df = pd.read_csv(url)
    return df['Net Asset Value'][0]
# nav_on('112958', '2018-11-06')

# %%
def nav_all_on(df, date):
    """Return a series containing NAV of all mutual fund on the given date.

    Return the NAV for the given date for all the mutual funds in the dataframe.

    Arguments:
    df: contains the mutual fund code in `quandl_fund_code` column
    date: date(in string form) for which the NAVs need to be returned
    |quandl_fund_code|
    """
    return (df['quandl_fund_code'].apply(lambda v: nav_on(v, date)))


def nav_diff(mf_code, start_date, end_date):
    """Return difference between NAV on start_date and on end_date."""
    return nav_on(mf_code, start_date) - nav_on(mf_code, end_date)
#nav_diff('122639', '2018-01-01', '2018-03-01')


def parse_fund_list(filename):
    """parse the list of mutual funds from file and return list of fund codes.

    Expected format of each line in the file:
    short fund name, fund code, full fund name, fund type
    If the line starts with # character, it's skipped.

    Arguments:
    filename: name of the file that contains the list of funds, one per line
    """
    # TODO: rewrite using pd.read_csv
    mf_codes = []
    with open(filename, 'r') as file:
        for line in file:
            if line[0] != '#':
                mf_codes.append(line.rstrip().split(','))
    return mf_codes
#parse_fund_list('fund_list.csv')


def full_df(fund_list='fund_list.csv', transactions='all_transactions.csv'):
    """Merge and return the two datasets from the passed files.

    Funds list is read from `fund_list` file and transaction list is read from
    `transactions` file and the two datasets are merged and the result is
    returned as a dataframe. In the current version, Franklin India Smaller
    Companies fund is dropped as it has a very short life in my portfolio.
    Current version doesn't support the concept of both buy and sell states of
    the funds; the assumption is funds are bought and then held forever.

    Arguments:
    fund_list: Name of the file that contains list of mutual funds
    transactions: Name of  the file that contains list of all transactions

    Returns:
    |fund_name|date|type|amount|nav|units|quandl_f_code|full_fund_name|fund_type
    """
    df1 = pd.read_csv(fund_list,
                      skiprows=1,
                      names=['fund_name','quandl_fund_code',
                             'full_fund_name','fund_type'])
    df2 = pd.read_csv(transactions)
    # HACK: fisc shouldn't be hardcoded to be skipped like this
    df2 = df2[df2.fund_name != 'fisc']
    return df2.merge(df1, how='left', on='fund_name')


# TODO: convert the arguments into an options structure
def prep_df(mf_code, collapse, start_date, end_date, scale):
    if end_date is None:
        end_date = dt.datetime.today().strftime('%Y-%m-%d')
    if start_date is None:
        start_date = dt.datetime.today() - dt.timedelta(days=30)
        start_date = start_date.strftime('%Y-%m-%d')
    base_url = 'https://www.quandl.com/api/v3/datasets/AMFI/'
    static = '.csv?api_key=A-j1TXe6-_yv9sU7CRf2&transform=normalize&collapse='
    url = (base_url + mf_code + static + collapse + '&start_date=' + start_date
           + '&end_date=' + end_date)

    df = pd.read_csv(url,
                     header=None,
                     sep=',',
                     skiprows=1,
                     names=['date', 'amt', '', ''],
                     usecols=['date', 'amt'],
                     index_col='date').iloc[::-1]
    #print(df)
    df['amt'] = df['amt'] * scale
    return df


def prep_main_df(mf_codes, collapse, start_date, end_date=None, scale=1000):

    def display_date(d):
        return dt.datetime.strptime(d, '%Y-%m-%d').strftime('%d %b %y')

    # easier if dfs is a list?
    dfs = {}
    for name,code,_,_ in mf_codes:
        dfs[name] = prep_df(code,
                            collapse=collapse,
                            start_date=start_date,
                            end_date=end_date,
                            scale=scale)

    # concat is simpler if 'date' is index in each of the dataframes
    df = pd.concat(dfs, axis=1)
    df.columns = df.columns.droplevel(level=1)
    df = df.dropna(axis='index')
    # apply() is easier to use if 'date' is a normal column
    df.index.names = ['date']
    df = df.reset_index()
    df['display_date'] = df['date'].apply(display_date)
    return df

def refresh_dfs(mf_codes):
    df_daily = prep_main_df(mf_codes,
                            collapse='daily',
                            start_date='2018-09-01')
    df_daily.to_csv('df_cache/daily_all_df.csv', index=False)

    df_weekly = prep_main_df(mf_codes,
                             collapse='weekly',
                             start_date='2018-01-01')
    df_weekly.to_csv('df_cache/weekly_all_df.csv', index=False)

    df_monthly = prep_main_df(mf_codes,
                              collapse='monthly',
                              start_date='2018-01-01')
    df_monthly.to_csv('df_cache/monthly_all_df.csv', index=False)

    df_quarterly = prep_main_df(mf_codes,
                                collapse='quarterly',
                                start_date='2018-01-01')
    df_quarterly.to_csv('df_cache/quarterly_all_df.csv', index=False)

    df_annual = prep_main_df(mf_codes,
                             collapse='annual',
                             start_date='2016-01-01')
    df_annual.to_csv('df_cache/annual_all_df.csv', index=False)

    return True
