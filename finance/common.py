import datetime as dt

def yesterday():
    """Return yesterday's date in the string format."""
    return (dt.datetime.now() - dt.timedelta(days=3)).strftime('%Y-%m-%d')
#yesterday()

def format_rupees(v):
    # Format a numeric value as Indian rupees and remove decimal part by default
    return locale.currency(v, grouping=True)
