import functools as ft

# TODO: return different colours for multiple different thresholds
#       e.g. dark red for low thr, light red for med thr, etc.
def highlight_big_values(val, low_thr=-1500, high_thr=1500):
    '''
    highlight the big values in a Series yellow.
    '''
    if val <= low_thr:
        return 'color: red'
    elif val >= high_thr:
        return 'color: lime'
    return ''

def create_table(df, fund_names, out_file, table_name=''):
    diff = df.drop(['date', 'display_date'], axis='columns')
    diff['all'] = diff.sum(axis='columns', numeric_only=True)
    # following order is very important, think twice before changing it
    pct = diff['all'].pct_change()  # do before diff is calculated on columns
    pct_from_init = (diff['all'] - diff.loc[0,'all']) / diff.loc[0, 'all']
    # TODO: take a dataframe and a list of column names and return df with
    #       diff calculated only on those columns
    diffs = diff.diff()
    diffs.iloc[0] = 0 # do before astype() is called on all columns
    print(diffs)
    diffs = diffs.astype('int') # do before pct is added to dataframe
    diffs['%'] = pct.round(2)
    diffs['%2'] = pct_from_init.round(2)
    diffs.loc[0,'%'] = diffs.loc[0, '%2'] = 0

    diffs['date'] = df['display_date']
    # TODO: make threshold values customisable from outside create_table
    da = (diffs.style.applymap(ft.partial(highlight_big_values,
                                          low_thr=-1000,
                                          high_thr=1000),
                               subset=fund_names))
    da = (da.applymap(ft.partial(highlight_big_values,
                                low_thr=-5000,
                                high_thr=5000),
                     subset=['all']))
    da = (da.applymap(ft.partial(highlight_big_values,
                                low_thr=-0.01,
                                high_thr=0.01),
                      subset=['%']))
    da.set_caption(table_name + ' gain/loss table')
    with open (out_file,'w') as out:
        out.write(da.render())
    return da


def loss_gains(df, date):
    df['nav_diffs'] = df['nav_current'] - df['nav']
    df['amount_diffs'] = df['nav_diffs'] * df['units']
    # formatting should be done right before displaying
    #df['amount_diffs'] = df['amount_diffs'].apply(format_rupees)
    return df['amount_diffs']


def current_values(df):
    df['current_values'] = df['nav_current'] * df['units']
    # formatting should be done right before displaying
    #df['current_values'] = df['current_values'].apply(format_rupees)
    #df['amount_formatted'] = (df['amount'].apply(format_rupees))
    return df['current_values']


def consolidate_same_funds(df):
    return df.groupby('full_fund_name').sum()
