import urllib.request as req
import bs4 as bs4
import os as os
import csv


# try/catch for AttributeError
def get_labels(bs):
    header = bs.find('div', class_='transaction-header').find_all('div')
    labels=[]
    for d in header:
        labels.append(d.get_text())
    return labels


def get_data(bs, labels):
    all_tr = bs.select('div.transaction.hidden-xs')
    rows = []
    for tr in all_tr:
        details = tr.find_all('div')
        row = {}
        for key,d in zip(labels,details):
            v = d.get_text()
            if key == 'Fund':
                v = v.strip()
            if key == 'Amount':
                v = v.replace('₹ ', '')
            row[key] = v
            rows.append(row)
    return rows


def main():
    with open('kuvera_transactions.html', 'r') as kt:
        bs = bs4.BeautifulSoup(kt)

    labels = get_labels(bs)
    rows = get_data(bs, labels)
    with open('kuvera_trs.csv', 'w') as ktrs:
        w = csv.writer(ktrs)
        w.writerow(labels)
        for r in rows:
            #for v in r.values():
            w.writerow(r.values())


main()
