# %%
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import datetime as dt
import locale
locale.setlocale(locale.LC_MONETARY, 'en_IN')
import pandas.io.formats.style as st
import seaborn as sns
#sns.set_style('darkgrid')
import functools as ft
from core import *
from common import *
from output import *

# data only available from 2016/09/01?

#%%
pd.options.display.float_format = '{0:,.0f}'.format
df = full_df()
df2 = df.drop(['fund_name','type', 'fund_type'], axis='columns')
df2['nav_current'] = nav_all_on(df, yesterday())
lg = loss_gains(df2, yesterday())
cv = current_values(df2)
res = pd.concat([df2[['date','full_fund_name','amount']],
                 current_values(df2),
                 loss_gains(df2, yesterday())], axis='columns')
res['amount'] = res['amount'].apply(lambda v: np.float(v))
res

cg = consolidate_same_funds(res[['full_fund_name', 'amount', 'current_values']])
cg

#%%
mf_codes = parse_fund_list('fund_list.csv')
mf_codes
refresh_dfs(mf_codes)

#%%
df_daily = pd.read_csv('df_cache/daily_all_df.csv')
df_weekly = pd.read_csv('df_cache/weekly_all_df.csv')
df_monthly = pd.read_csv('df_cache/monthly_all_df.csv')
df_quarterly = pd.read_csv('df_cache/quarterly_all_df.csv')
df_annual = pd.read_csv('df_cache/annual_all_df.csv')

mf_codes = parse_fund_list('fund_list.csv')
fund_names = [mf_code[0] for mf_code in mf_codes]

create_table(df_daily, fund_names, 'build/daily.html', 'Daily')
create_table(df_weekly, fund_names, 'build/weekly.html', 'Weekly')
create_table(df_monthly, fund_names, 'build/monthly.html', 'Monthly')
create_table(df_quarterly, fund_names, 'build/quarterly.html', 'Quarterly')
create_table(df_annual, fund_names, 'build/annual.html', 'Annual')
