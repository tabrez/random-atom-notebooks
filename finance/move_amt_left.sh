#!/bin/bash

file='transactions.csv'
while read -r line
do
  str1="$(echo $line | cut -d ',' -f 1-3)"
  str2="$(echo $line | cut -d ',' -f 4,5)"
  str3="$(echo $line | cut -d ',' -f 6)"
  echo "$str1,$str3,$str2"
done < "$file"
