# likely obsolete
def nav_diffs(df):
    """return diff in NAVs on specified date and yesterday for all mfs.

    For all the mutual funds in the dataframe, calculate and return the
    difference in NAVs as on the date present in date column in the df and NAV
    of yesterday.

    df: The dataframe containing the mutual funds whose NAV differences are
        calculated. The code of the mutual fund is read from `quandl_fund_code`
        column and the start date is read from the `date` column.
    | quandl_fund_code | date |
    """
    def calc_nav_diffs(row):

        return nav_diff(str(row.quandl_fund_code),
                                   row.date,
                                   yesterday())

    return df.apply(calc_nav_diffs, axis=1)
nd = nav_diffs(full_df().iloc[:,:4])
df['nav_diffs'] = nd
df


# likely obsolete
def current_values(df):
    """Return the current value of all the mutual fund investments.

    For all the mutual funds in the dataframe, find their respective current
    values as per yesterday's NAV.

    Arguments:
    df: dataframe that contains the mutual fund code in quandl_fund_code column
    | quandl_fund_code |
    """
    def current_value(row):
        return row.units * nav_on(str(row.quandl_fund_code), yesterday())
    return df.apply(current_value, axis=1)
current_values(full_df())


def plot_movement(df):
    df2 = df.melt(id_vars=['date', 'display_date'],
                value_vars=fund_names,
                var_name='funds',  value_name='vals')

    five_thirty_eight = [
        "#30a2da",
        "#fc4f30",
        "#e5ae38",
        "#6d904f",
        "#8b8b8b",
    ]

    sns.set_palette(five_thirty_eight)
    plt.rcParams['figure.figsize']=(18,12)

    #fund_full_names = get from mf_codes, 3rd column
    #legend_labels = fund_full_names
    with sns.plotting_context('notebook',
                            font_scale=1.5,
                            rc={"lines.linewidth": 2.5}):
        ax = sns.lineplot(x="date", y="vals", hue='funds', data=df2)
        ax.set_xticklabels(labels=df2['display_date'].tolist())
        handles, labels = ax.get_legend_handles_labels()
        #ax.legend(handles=handles[1:], labels=legend_labels)
        sns.despine(right=True)
    plt.xticks(rotation='vertical')

    #l = plt.legend(loc='upper right', prop={'size': 22})
    plt.show()
