"""Test how concat works with multiindex labels."""

# %%
import pandas as pd
s1 = pd.Series([1, 2, 3], index=['a', 'b', 'c'])
s2 = pd.Series([11, 22, 33], index=['a', 'b', 'c'])
pd.concat([s1, s2], join='inner', axis='columns')
