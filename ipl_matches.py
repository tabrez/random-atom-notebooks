"""Analyse Match dataset of IPL tournament."""

# %%
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns
sns.set_style('whitegrid')
sns.set_context('notebook', font_scale=1.5)
mpl.rcParams.update({'figure.figsize': (14, 7)})

matches_path = '~/.kaggle/datasets/raghu07/ipl-data-till-2017/Match.csv'
matches = pd.read_csv(matches_path)


# %%
matches.isnull().sum().sum()
matches = matches.dropna(axis='index')
matches.info()
matches.head()

# %%
# how many matches were played each year
matches['Match_SK'].groupby(matches['Season_Year']).size()
# Players who won highest number of Man of the Match awards
# needs cleanup e.g. typo in ManOfMach & inconsistent naming convention
MoMs = matches.groupby('ManOfMach')['Match_SK'] \
        .size() \
        .sort_values(ascending=False) \
        .head(10)
plt.xticks(rotation=90)
sns.barplot(MoMs.index, MoMs.values)

# %%
# Number of matches played by each team
team1 = matches['Team1'].value_counts()
team2 = matches['Team2'].value_counts()
matches_played = team1 + team2
matches_played.name = 'matches_played'
# teams.head()
# teams.sum()
plt.xticks(rotation=70)
sns.barplot(matches_played.index, matches_played.values)

# %%
# Number of wins and win percentage of each team
# matches = matches[]
matches_won = matches['match_winner'].value_counts()
matches_won.name = 'matches_won'
win_perc = pd.concat([matches_played, matches_won], axis='columns')
# should be in clean section
# win_perc = win_perc.dropna(axis='index')
win_perc['win%'] = (win_perc['matches_won'] / win_perc['matches_played']) * 100
win_perc = win_perc.sort_values(by='win%', ascending=False)

# %%
# add a new column to represent which team batted first
matches['Toss_Name'].unique()
# should be in clean section
matches = matches.dropna(subset=['Toss_Name'], axis='index')
bat_f = matches[matches['Toss_Name'].isin(['bat', 'Bat'])]
bat_f['bat_first_team'] = bat_f['Toss_Winner']
field_f = matches[matches['Toss_Name'].isin(['field', 'Field'])]
field_f['bat_first_team'] = (field_f['Team2']
                             .where(field_f['Toss_Winner'] == field_f['Team1'],
                                    field_f['Team1']))
m = pd.concat([bat_f, field_f])
matches.count()
m = (m[['Team1', 'Team2', 'Toss_Winner',
     'Toss_Name', 'bat_first_team', 'match_winner']])
m

# %%
# add a column to indicate winning team batted or fielded first
m['bat_or_field'] = (np.where(m['bat_first_team'] == m['match_winner'],
                     'field first', 'bat_first'))
m

# %%
# display win count when batting first vs. fielding first
m['match_winner'].value_counts()
m = m[m['match_winner'] != 'tied']

winsbf = (m[m['match_winner'] == m['bat_first_team']]['match_winner']
          .value_counts())
winsbf.name = 'bat_first'
# s1 = pd.Series('bat_first', index=winsbf.index, name='bat_or_field')
# winsbf = (pd.concat([winsbf, s1], axis='columns'))

winsff = (m[m['match_winner'] != m['bat_first_team']]['match_winner']
          .value_counts())
winsff.name = 'field_first'
# s2 = pd.Series('field_first', index=winsff.index, name='bat_or_field')
# winsff = (pd.concat([winsff, s2], axis='columns'))

wins = pd.concat([winsbf, winsff, matches_won, win_perc['win%']], axis=1)
wins['wins_bat_first'] = (wins['bat_first'] / wins['matches_won']) * 100
wins['wins_field_first'] = (wins['field_first'] / wins['matches_won']) * 100
wins

# %%
# plot win count when batting first vs. fielding first
plt.xticks(rotation=90)
sns.countplot(x='match_winner', hue='bat_or_field', data=m)
