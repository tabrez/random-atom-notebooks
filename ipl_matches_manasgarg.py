"""Analyse Match dataset of IPL tournament."""

# %%
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns
sns.set_style('whitegrid')
sns.set_context('notebook', font_scale=1.5)
mpl.rcParams.update({'figure.figsize': (14, 7)})

# %%
matches_path = '~/.kaggle/datasets/manasgarg/ipl/matches.csv'
matches = pd.read_csv(matches_path)


# %%
matches.isnull().sum().sum()
matches = matches.drop(['umpire3'], axis='columns')
matches = matches.dropna(axis='index')
matches.info()
matches.head()

# %%
# how many matches were played each season
matches['id'].groupby(matches['season']).size()
# Players who won highest number of Man of the Match awards
MoMs = (matches.groupby('player_of_match')['id']
        .size()
        .sort_values(ascending=False)
        .head(10))
plt.xticks(rotation=90)
plt.yticks(np.arange(0, 20, 4))
sns.barplot(MoMs.index, MoMs.values)

# %%
# Number of matches played by each team overall
team1 = matches['team1'].value_counts()
team2 = matches['team2'].value_counts()
matches_played = team1 + team2
matches_played.name = 'matches_played'
plt.xticks(rotation=70)
sns.barplot(matches_played.index, matches_played.values)

# %%
# Number of matches played by each team in 2017
team1_2017 = matches[matches['season'] == 2017]['team1'].value_counts()
team1_2017
team2_2017 = matches[matches['season'] == 2017]['team2'].value_counts()
team2_2017
matches_played_2017 = team1_2017 + team2_2017
matches_played_2017

# %%
# Number of matches played by each team per season
team1_ps = matches.groupby('season')['team1'].value_counts()
team1_ps.index = team1_ps.index.rename('team', level=1)
team1_ps
team2_ps = matches.groupby('season')['team2'].value_counts()
team2_ps.index = team2_ps.index.rename('team', level=1)
team2_ps
matches_played_ps = team1_ps + team2_ps
matches_played_ps.name = 'matches_played_per_season'
matches_played_ps

# %%
# Number of wins and win percentage of each team
matches_won = matches['winner'].value_counts()
matches_won.name = 'matches_won'
win_perc = pd.concat([matches_played, matches_won], axis='columns')
win_perc['win%'] = (win_perc['matches_won'] / win_perc['matches_played']) * 100
win_perc = win_perc.sort_values(by='win%', ascending=False)
win_perc

# %%
# Number of wins and win percentage of each team each season
matches_won_ps = matches.groupby('season')['winner'].value_counts()
matches_won_ps.name = 'matches_won_per_season'
matches_won_ps.index = matches_won_ps.index.rename('team', level=1)
matches_won_ps
win_perc_ps = (pd.concat([matches_played_ps,
               matches_won_ps],
               axis='columns'))
win_perc_ps
win_perc_ps['win%'] = ((win_perc_ps['matches_won_per_season']
                        / win_perc_ps['matches_played_per_season']) * 100)
# win_perc_ps = (win_perc_ps.sort_index(by='win%',
#                                        ascending=False))
win_perc_ps

# %%
# add a new column to represent which team batted first
matches['toss_decision'].unique()
bat_f = matches[matches['toss_decision'].isin(['bat', 'Bat'])]
bat_f['bat_first_team'] = bat_f['toss_winner']
field_f = matches[matches['toss_decision'].isin(['field', 'Field'])]
field_f['bat_first_team'] = (field_f['team2']
                             .where(field_f['toss_winner'] == field_f['team1'],
                                    field_f['team1']))
m = pd.concat([bat_f, field_f])
matches.count()
m = (m[['team1', 'team2', 'toss_winner',
     'toss_decision', 'bat_first_team', 'winner']])
m

# %%
# add a column to indicate winning team batted or fielded first
m['winner_bat_or_field'] = (np.where(m['bat_first_team'] == m['winner'],
                            'field_first', 'bat_first'))
m

# %%
# display win count when batting first vs. fielding first
m['winner'].value_counts()
# m = m[m['winner'] != 'tied']

winsbf = (m[m['winner'] == m['bat_first_team']]['winner']
          .value_counts())
winsbf.name = 'bat_first'

winsff = (m[m['winner'] != m['bat_first_team']]['winner']
          .value_counts())
winsff.name = 'field_first'

wins = pd.concat([winsbf, winsff, matches_won, win_perc['win%']], axis=1)
wins['wins_bat_first'] = (wins['bat_first'] / wins['matches_won']) * 100
wins['wins_field_first'] = (wins['field_first'] / wins['matches_won']) * 100
wins

# %%
# calculate ratio of wins batting first and wins fielding first
wins_batting_first = m[m['winner_bat_or_field'] == 'bat_first'].count()[0]
wins_fielding_first = m[m['winner_bat_or_field'] == 'field_first'].count()[0]
bat_field_win_ratio = wins_batting_first / wins_fielding_first
bat_field_win_ratio

# %%
# plot win count when batting first vs. fielding first
plt.xticks(rotation=90)
sns.countplot(x='winner', hue='winner_bat_or_field', data=m)
