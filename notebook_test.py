"""This module tests Python packages."""


# %%
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
# %matplotlib inline
# %config InlineBackend.figure_format = 'retina'
mpl.rcParams.update({'figure.figsize': (14, 7)})

# %%
np.sqrt(25)
x = np.random.randint(0, 1000, size=1000)
plt.hist(x)

# %%
y = np.random.randint(1000, 2000, 100)
plt.hist(y)

np.random.randint(1000, 2000, 100)
np.random.rand(399)


def add(x, y):
    """Add two numbers and return the sum."""
    return x + y

# %%
add(3, 4)
