"""
Seaborn tutorial.

Distributions tutorial:https://seaborn.pydata.org/tutorial/distributions.html"
"""

import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns
from scipy import stats
mpl.rcParams.update({'figure.figsize': (18, 10)})
np.random.seed(sum(map(ord, "distributions")))


# univariate distributions

# %%
# By default, this will draw a histogram and fit a
# kernel density estimate (KDE).
x = np.random.normal(size=100)
sns.distplot(x, bins=20, kde=False, rug=True, fit=stats.gamma)
sns.distplot(x, hist=False, rug=True)

# bivariate distributions

# %%
# The most familiar way to visualize a bivariate distribution is a scatterplot,
# which is the default kind of plot shown by the jointplot() function
mean, cov = [0, 1], [(1, .5), (.5, 1)]
data = np.random.multivariate_normal(mean, cov, 200)
df = pd.DataFrame(data, columns=["x", "y"])
sns.jointplot(x='x', y='y', data=df)


# %%
# The bivariate analogue of a histogram is known as a “hexbin” plot, because
# it shows the counts of observations that fall within hexagonal bins. This
# plot works best with relatively large datasets.
data = np.random.multivariate_normal(mean, cov, 1000)
df = pd.DataFrame(data, columns=["x", "y"])
sns.jointplot(x='x', y='y', data=df, kind='hex')


# %%
# You can also draw a two-dimensional kernel density plot with the kdeplot()
# function. This allows you to draw this kind of plot onto a specific (and
# possibly already existing) matplotlib axes, whereas the jointplot() function
# manages its own figure
f, ax = plt.subplots(figsize=(6, 6))
sns.kdeplot(df.x, df.y, ax=ax)
sns.rugplot(df.x, color="g", ax=ax)
sns.rugplot(df.y, vertical=True, ax=ax)


# %%
# If you wish to show the bivariate density more continuously, you can simply
# increase the number of contour levels
f, ax = plt.subplots(figsize=(6, 6))
cmap = sns.cubehelix_palette(as_cmap=True, dark=0, light=1, reverse=True)
sns.kdeplot(df.x, df.y, cmap=cmap, n_levels=60, shade=True)


# %%
# To plot multiple pairwise bivariate distributions in a dataset, you can use
# the pairplot() function
iris = sns.load_dataset('iris')
sns.pairplot(iris)


# %%
# the pairplot() function is built on top of a PairGrid object, which can be
# used directly for more flexibility
g = sns.PairGrid(iris)
g.map_diag(sns.kdeplot)
g.map_offdiag(sns.kdeplot, cmap="Blues_d", n_levels=6)
