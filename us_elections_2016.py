"""Data analysis of 2012 US Election donations dataset."""

# %%
import pandas as pd
fec = pd.read_csv('~/datasets/P00000001-ALL.csv')
fec.isnull().sum()
fec.info()
fec.head()

# %%
fec.cmte_id.unique()
fec.cand_id.unique()
fec.cand_nm.unique()
fec.contbr_nm.unique().size
fec.contbr_city.unique().size
fec.contbr_st.unique().size
fec.contbr_employer.unique().size
fec.contbr_occupation.unique().size
# average amount of donation per donator
fec.contb_receipt_amt.sum() / fec.contb_receipt_amt.count()
# range of donation amounts
fec.contb_receipt_amt.min(), fec.contb_receipt_amt.max()
# how many positve vs. negative donations
neg = fec[fec.contb_receipt_amt < 0]['contb_receipt_amt'].count()
zero = fec[fec.contb_receipt_amt == 0]['contb_receipt_amt'].count()
pos = fec[fec.contb_receipt_amt > 0]['contb_receipt_amt'].count()
neg, zero, pos
(fec.contb_receipt_amt > 0).value_counts()
# remove negative and zero donation entries
fecp = fec[fec.contb_receipt_amt > 0]
# recalculate average amount of donation
fecp.contb_receipt_amt.sum() / fecp.contb_receipt_amt.count()
# time period during which donations were made
fecp.contb_receipt_dt.min(), fecp.contb_receipt_dt.max()
fecp.receipt_desc.unique().size
fecp.receipt_desc.dropna().head(10)
fecp.memo_cd.unique()
fecp.memo_cd.dropna().head(10)
fecp.memo_text.unique().size
fecp.memo_text.dropna().head(10)
fecp.form_tp.unique()
fecp.form_tp.head(15)
fecp.file_num.unique()

# %%
# I decide to drop the text fields containing descriptions
fecp = (fecp.drop(['contbr_zip',
                   'receipt_desc',
                   'memo_cd',
                   'memo_text',
                   'file_num'], axis='columns'))
fecp.iloc[0]

# %%
# do donators donate multiple times?
fecp.contbr_nm.unique().size
fecp.contbr_nm.count()
# Highest and lowest donators
total_ds = (fecp
            .groupby('contbr_nm')['contb_receipt_amt']
            .sum()
            .sort_values(ascending=False))
highest = total_ds.head(20)
lowest = total_ds.tail(20)
highest, lowest

# %%
# create a subset of the dataset for the top 2 candidates of 2012 Elections:
# Barack Obama and Mitt Romney
top2_cand = fecp[fecp.cand_nm.isin(['Obama, Barack', 'Romney, Mitt'])]
top2_cand.cand_nm.unique()

# %%
# map each candidate name to their respective political party name and
# add the party name to main dataset
parties = {'Bachmann, Michelle': 'Republican',
           'Cain, Herman': 'Republican',
           'Gingrich, Newt': 'Republican',
           'Huntsman, Jon': 'Republican',
           'Johnson, Gary Earl': 'Republican',
           'McCotter, Thaddeus G': 'Republican',
           'Obama, Barack': 'Democrat',
           'Paul, Ron': 'Republican',
           'Pawlenty, Timothy': 'Republican',
           'Perry, Rick': 'Republican',
           "Roemer, Charles E. 'Buddy' III": 'Republican',
           'Romney, Mitt': 'Republican',
           'Santorum, Rick': 'Republican'}
# fecp.iloc[150000:450000].cand_nm.map(parties)
fecp['party_name'] = fecp.cand_nm.map(parties)
fecp.party_name.unique()
fecp[['cand_nm', 'party_name']].head(15)

# %%
# total amount of donations for candidates of each party
pd.options.display.float_format = '${:,.2f}'.format
don = fecp.groupby('party_name').contb_receipt_amt.sum()
don.Republican / don.Democrat
