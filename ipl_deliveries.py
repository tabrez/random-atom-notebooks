"""Analyse Match dataset of IPL tournament."""

# %%
import numpy as np
import pandas as pd
import matplotlib as mpl
# import matplotlib.pyplot as plt
import seaborn as sns
sns.set_style('whitegrid')
sns.set_context('notebook', font_scale=1.5)
mpl.rcParams.update({'figure.figsize': (14, 7)})

matches_path = '~/.kaggle/datasets/manasgarg/ipl/deliveries.csv'
d = pd.read_csv(matches_path)

# %%
d.isnull().sum()

# %%
# matches = matches.dropna(axis='index')
d.info()
d.head()

# %%
# sort a series in decreasing order and return top n rows
def top_n(s, n):
    return s.sort_values(ascending=False)[:n]

# %%
# find the number of matches played
d.match_id.unique().size
# find the number of teams
pd.concat([d.batting_team, d.bowling_team]).unique().size
# find the number of players
f = d.fielder.dropna()
(pd.concat([d.batsman, d.bowler, d.non_striker, f]).unique().size)
# find the number of runs scored by each player, sort top 10
d.groupby('batsman')['batsman_runs'].sum().sort_values().tail(10).iloc[::-1]
# what are the different ways of player dismissal
d.dismissal_kind.unique()

# %%
# find the number of wickets taken by each player, sort top 10
bowler_d = d[~d['dismissal_kind']
             .isin(['run out',
                    'hit wicket',
                    'obstructing the field',
                    'retired hurt'])]
# bowler_d = bowler_d.dropna(subset=['dismissal_kind'])
bowler_d.dismissal_kind.unique()
(bowler_d.groupby('bowler')['dismissal_kind']
         .count()
         .sort_values(ascending=False)
         .head(20))

# %%
# find the number of matches played by each batsman
d['match_id'].unique()
d['batsman'].unique()
top_n(d.groupby('batsman')['match_id'].unique().apply(lambda g: g.size), 10)
top_n(d.groupby('batsman')['match_id'].nunique(), 10)

# %%
# find the number of matches played by each bowler
top_n(d.groupby('bowler')['match_id'].nunique(), 10)

# %%
# find the number of matches played by each player
# bt_mt_count = d.groupby('batsman')['match_id'].unique()
# bw_mt_count = d.groupby('bowler')['match_id'].unique()
# nstr_mt_count = d.groupby('non_striker')['match_id'].unique()
# fl_mt_count = d.groupby('fielder')['match_id'].unique()
# bt_mt_count.index = bt_mt_count.index.rename('player')
# bt_mt_count
# bw_mt_count.index = bw_mt_count.index.rename('player')
# bw_mt_count
# nstr_mt_count.index = nstr_mt_count.index.rename('player')
# nstr_mt_count
# fl_mt_count.index = fl_mt_count.index.rename('player')
# fl_mt_count
#
# bt_mt_count.where(bt_mt_count > bw_mt_count, bw_mt_count)

# %%
# find the number of matches played by each player
bt = (pd.DataFrame(np.unique(d[['match_id', 'batsman']]
                             .values
                             .astype(str), axis=0),
                   columns=['match_id', 'player']))
bw = (pd.DataFrame(np.unique(d[['match_id', 'bowler']]
                             .values
                             .astype(str), axis=0),
                   columns=['match_id', 'player']))
nstr = (pd.DataFrame(np.unique(d[['match_id', 'non_striker']]
                             .values
                             .astype(str), axis=0),
                   columns=['match_id', 'player']))
fl = (pd.DataFrame(np.unique(d[['match_id', 'fielder']]
                             .values
                             .astype(str), axis=0),
                   columns=['match_id', 'player']))
mat_by_players = (pd.DataFrame(np.unique(pd.concat([bt, bw, nstr, fl])
                                         .values
                                         .astype(str), axis=0),
                               columns=['match_id', 'player']))
mat_by_players = mat_by_players[mat_by_players['player'] != 'nan']
top_n(mat_by_players.groupby('player')['match_id'].count(), 15)

# %%
# verify above result
def how_many(player, d):
    bhajji = player
    s = d[['batsman', 'bowler', 'fielder', 'non_striker', 'match_id']]
    bhajji_bat = s[s['bowler'] == bhajji]
    bhajji_bowl = s[s['batsman'] == bhajji]
    bhajji_fld = s[s['fielder'] == bhajji]
    bhajji_nstr = s[s['non_striker'] == bhajji]
    bhajji_all = pd.concat([bhajji_bat, bhajji_bowl, bhajji_fld, bhajji_nstr])
    bhajji_all = bhajji_all.groupby('match_id').head(1)
    bw = bhajji_all[(bhajji_all['bowler'] == bhajji)].count()[0]
    bt = bhajji_all[(bhajji_all['batsman'] == bhajji)].count()[0]
    fl = bhajji_all[(bhajji_all['fielder'] == bhajji)].count()[0]
    nstr = bhajji_all[(bhajji_all['non_striker'] == bhajji)].count()[0]
    return (bw, bt, fl, nstr)

sum(how_many('SK Raina', d))

# %%
# build a dataset relating match id with player names along with
# the data to show if they batted, bowled, fielded, etc. in that match
# d[d.columns.difference(['batsman', 'bowler', 'fielder', 'non-non_striker'])]
bt = (pd.DataFrame(np.unique(d[['match_id', 'batsman']]
                             .values
                             .astype(str), axis=0),
                   columns=['match_id', 'player']))
bt['batting'] = True
bw = (pd.DataFrame(np.unique(d[['match_id', 'bowler']]
                             .values
                             .astype(str), axis=0),
                   columns=['match_id', 'player']))
bw['bowling'] = True
nstr = (pd.DataFrame(np.unique(d[['match_id', 'non_striker']]
                             .values
                             .astype(str), axis=0),
                   columns=['match_id', 'player']))
nstr['non_striker'] = True
fl = (pd.DataFrame(np.unique(d[['match_id', 'fielder']]
                             .values
                             .astype(str), axis=0),
                   columns=['match_id', 'player']))
fl['fielder'] = True
mat_by_players = pd.concat([bt, bw, nstr, fl])
mat_by_players = mat_by_players[mat_by_players['player'] != 'nan']
mat_by_players.count()
mat_by_players = mat_by_players.fillna(False)
mat_by_players = mat_by_players[['match_id',
                                 'player',
                                 'batting',
                                 'bowling',
                                 'fielder',
                                 'non_striker']]
mat_by_players
