"""
Tutorial on matplotlib by Nicolas P. Rougier:
http://www.labri.fr/perso/nrougier/teaching/matplotlib/
"""

# %%
import numpy as np
import matplotlib.pyplot as plt
# draw the cosine and sine functions on the same plot using 256 points from
# -np.ip to np.pi; use the default matplotlib settings
X = np.linspace(-np.pi, np.pi, 256, endpoint=True)
C,S = np.cos(X), np.sin(X)

plt.plot(X, C)
plt.plot(X, S)

# %%
# use default values for important matplotlib settings and then experiment with
# them.

# Create a new figure of size 8x6 points, using 100 dots per inch
plt.figure(figsize=(10,6), dpi=100)
# Set x limits
# plt.xlim(-4.0, 4.0)
plt.xlim(X.min()*1.1, X.max()*1.1)
# Set x ticks
# plt.xticks(np.linspace(-4,4,9,endpoint=True))
plt.xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi],
           [r'$-\pi$', r'$-\pi/2$', r'$0$', r'$+\pi/2$', r'$+\pi$'])
# Set y limits
# plt.ylim(-1.0, 1.0)
plt.ylim(C.min()*1.1, C.max()*1.1)
# Set y ticks
plt.yticks([-1, 0, 1], [r'$-1$', r'$0$', r'$+1$'])

# Hide two spines and move the other two spines to the centre
ax = plt.gca()
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.spines['left'].set_color('black')
ax.spines['bottom'].set_color('black')
# ax.xaxis.set_ticks_position('bottom')
ax.spines['bottom'].set_position('zero')
# ax.yaxis.set_ticks_position('left')
ax.spines['left'].set_position('zero')
ax.spines['left'].set_linewidth(2)
ax.spines['bottom'].set_linewidth(1)
ax.tick_params(axis='x', colors='#f06215')

# Make reading the labels easier
# for label in ax.get_xticklabels() + ax.get_yticklabels():
#     label.set_fontsize(16)
#     label.set_bbox(dict(fc='w', edgecolor='None', alpha=0.1))

ax.tick_params(labelsize=16, grid_color='red', )
# Plot cosine using blue color with a continuous line of width 1 (pixels)
plt.plot(X, C, color='blue', linewidth=2.5, linestyle='-', label='cosine')

# Plot sine color using green with a continuous line of width 1 (pixels)
plt.plot(X, S, color='red', linewidth=2.5, linestyle='-', label='sine')

# Add a legend
plt.legend(loc='upper left', frameon=False)

# Save figure using 72 dots per inch
# plt.savefig('./figures/exercise_1.png', dpi=72)
