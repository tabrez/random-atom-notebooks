"""
Seaborn tutorial.

Basic tutorial from https://seaborn.pydata.org/tutorial/aesthetics.html.
"""


import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns
mpl.rcParams.update({'figure.figsize': (18, 10)})
np.random.seed(sum(map(ord, "aesthetics")))


def sinplot(flip=1):
    """Draw a sine plot with different magnitudes and angle."""
    x = np.linspace(0, 14, 100)
    for i in range(1, 7):
        plt.plot(x, np.sin(x + i * 0.5) * (7 - i) * flip)


# %%
sns.set_style('ticks')
sns.set_palette("husl")
sns.set(color_codes=True)
sinplot()
sns.despine(left=True)
